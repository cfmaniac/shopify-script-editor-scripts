discount = 0 # initial discount
min_discount_order_amount = Money.new(cents:100) * 500 # $500 to cents, to fire the discount flag
total = Input.cart.subtotal_price_was
discount = Money.new(cents: 100) * 10 if total > min_discount_order_amount  #discount amount you are offering in cents
message = "Here's $10 off" #discount message shown to customer

Input.cart.line_items.each do |line_item|
  line_item.change_price(line_item.line_price - discount, message: message)
end

Output.cart = Input.cart