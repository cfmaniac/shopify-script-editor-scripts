MESSAGE = "Free Standard Shipping" #promotional message

Input.shipping_rates.each do |shipping_rate|
  next unless shipping_rate.source == "shopify"
  next unless shipping_rate.name == "Standard Shipping"
  shipping_rate.apply_discount(shipping_rate.price, message: MESSAGE)
end

Output.shipping_rates = Input.shipping_rates