# ================================ Customizable Settings ================================
# ================================================================
# Show/hide rates for specific products, where:
#   - 'product_types' is a list of product types to identify
#     associated products
#   - 'rate_mode' determines whether the entered rates
#     should be hidden, or shown.
#     Can be:
#       - ':hide' to hide them
#       - ':show' to show them
#   - 'rate_match_type' determines whether the below strings
#     should be an exact or partial match. Can be:
#       - ':exact' for an exact match
#       - ':partial' for a partial match
#   - 'rate_names' is a list of strings to identify rates
#     - if using ':all' above, this can be set to 'nil'
# ================================================================
SHOW_HIDE_RATES_FOR_PRODUCT_TYPE = [
  {
    product_types: ["Battery"],
    rate_mode: :hide,
    rate_match_type: :exact,
    rate_names: ["Priority Mail",
                 "USPS Priority Mail",
                 "USPS First Class Package w/Tracking - Estimated 2-4 Business Days, No Insurance",
                 "UPS Next Day Air® Early",
                 "USPS Priority Mail Express - Estimated 1-2 Business Days w/ Sunday Delivery",
                 "UPS Next Day Air®",
                 "UPS Next Day Air Saver®"
                 
                 ],
  },
]

# ================================ Script Code (do not edit) ================================
# ================================================================
# ProductTypeSelector
#
# Finds matching products by product type
# ================================================================
class ProductTypeSelector
  def initialize(types)
    @types = types.map { |type| type.downcase.strip }
  end

  def match?(line_item)
    @types.any? { |type| line_item.variant.product.product_type.downcase.strip == type }
  end
end

# ================================================================
# RateNameSelector
#
# Finds whether the supplied rate name matches any of the entered
# names
# ================================================================
class RateNameSelector
  def initialize(match_type, rate_names)
    @match_type = match_type
    @comparator = match_type == :exact ? '==' : 'include?'
    @rate_names = rate_names&.map { |rate_name| rate_name.downcase.strip }
  end
  
  def match?(shipping_rate)
    @rate_names.any? { |name| shipping_rate.name.downcase.send(@comparator, name) }
  end
end

# ================================================================
# ShowHideRatesForProductTypeCampaign
#
# If the cart contains the entered product and is being shipped
# to the entered country, delete all shipping rates
# ================================================================
class ShowHideRatesForProductTypeCampaign
  def initialize(product_types)
    @product_types = product_types
  end

  def run(cart, shipping_rates)
    @product_types.each do |product_type|
      product_selector = ProductTypeSelector.new(product_type[:product_types])

      next unless cart.line_items.any? { |line_item| product_selector.match?(line_item) }
      
      rate_name_selector = RateNameSelector.new(
        product_type[:rate_match_type],
        product_type[:rate_names],
      )

      shipping_rates.delete_if do |shipping_rate|
        rate_match = rate_name_selector.match?(shipping_rate)
        hide_rates = product_type[:rate_mode] == :hide ? true : false
        
        rate_match == hide_rates
        
       
      end
    end
  end
end


CAMPAIGNS = [
  ShowHideRatesForProductTypeCampaign.new(SHOW_HIDE_RATES_FOR_PRODUCT_TYPE),
]

CAMPAIGNS.each do |campaign|
  campaign.run(Input.cart, Input.shipping_rates)
end

Output.shipping_rates = Input.shipping_rates