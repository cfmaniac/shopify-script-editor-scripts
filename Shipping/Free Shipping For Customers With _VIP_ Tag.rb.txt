TAG = "vip" #customer tag
MESSAGE = "VIP Customer Promotion" #additional message
customer = Input.cart.customer

if customer
  if customer.tags.include?(TAG)
    Input.shipping_rates.each do |shipping_rate|
      if shipping_rate.name.include?("Insured Shipping and Handling (USPS Priority Express)")
        shipping_rate.change_name("FREE VIP GROUND SHIPPING (USPS Priority Express)", { message: "" })
        shipping_rate.apply_discount(shipping_rate.price, message: MESSAGE)
      end
    end
  end
end

Output.shipping_rates = Input.shipping_rates