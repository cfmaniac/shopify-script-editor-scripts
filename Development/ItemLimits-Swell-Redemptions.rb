# --------------------------------------#
# JH Latest Update; Auditing Product Line Item Limits:
# 11/10/2020
# Added Product ID, Product Variant Limits
# --------------------------------------#
#Products Limited to 1:
limit_to_one = [
                    "4362650583109", #N.O.7 Sample Packets
                    "4482366767173", #7 Serving Shredd-AF
                    "4489804054597", #7 Day Shreddeded-AF Trial
                    "4601964167237", #(SWEAT MOP - Free Gift)
                    "4601976160325", #(STEEL SHAKER - Free Gift) (edited)
                    "4604106145861" # HyperAde Sample Packet

              ];
#Products Limited to 2:
limit_to_two_prods = [];
#Product Variants Limited to 2:
limit_to_two_variants = [
                     "31667939967045", # 7 Serving Shreddeed-AF
                     "31847271235653", #Whey Pro Bundle Variants
                     "31847271137349",
                     "31847271170117",
                     "31847271202885",
                     "31847271268421",
                     "31847271301189",
                     "31125075951685", # Shredded-Af Bundle Variants
                     "31221075050565",
                     "31125075918917",
                     "31125075984453",
                     "31125076017221",
                     "31125076049989",
                     "31925839888453", # Skin Splitting Bundle Variants
                     "31925839953989",
                     "31925840019525",
                     "31925840085061",
                     "31925840150597",
                     "31934316118085", #HyperAde Sample Variants
                     "31934356914245",
                     "31221046837317", #Veg-Shred Bundle Variants
                     "31221755052101",
                     "31217319411781",
                     "31221046771781",
                     "31221046870085",
                     "31221046902853",
                     "31217576968261",
                     "31221755084869",
                     "31217576902725",
                     "31217576935493",
                     "31217577001029",
                     "31217577033797",
                     "31217577132101",
                     "31221755117637",
                     "31217577066565",
                     "31217577099333",
                     "31217577164869",
                     "31217577197637",
                     "31217577295941",
                     "31221755150405",
                     "31217577230405",
                     "31217577263173",
                     "31217577328709",
                     "31217577361477",
                     "31217577459781",
                     "31221755183173",
                     "31217577394245",
                     "31217577427013",
                     "31217577492549",
                     "31217577525317",
                     "31948468060229", #Skin Splitting Bundle Variants
                     "31948467961925",
                     "319948467929157",
                     "31948467994693",
                     "31948468027461"];

#Products Limited to 5:
limit_to_five = [     "4337854414917", #Preworkout Bundle
                      "4337854120005", #Shredded-AF Bundle
                      "4482366767173", #7 Serving Shredded-AF
                      "4489804054597", #7Day Shredded-AF
                      "4599285284933",  #Skin Splitting bundle
                      "4568073076805", #Whey Pro Bundle
                      "4337854414917", #Preworkout Bundle
                      "4337854120005", #Shredded-AF Bundle
                      "4355872587845", #Veg Shred Bundle
                      "4482366767173", #7 Serving Shredded-AF
                      "4489804054597", #7Day Shredded-AF
                      "4584553054277", #Smash & REst Bundle
                      "4590246821957", #Pre-Workout Bundle
                      "4590177321029", #Pre-Workout Bundle
                      "4610141388869" #Skin Splitting Bundle (Duplicate)
                ];

class Campaign
  def initialize(condition, *qualifiers)
    @condition = (condition.to_s + '?').to_sym
    @qualifiers = PostCartAmountQualifier ? [] : [] rescue qualifiers.compact
    @line_item_selector = qualifiers.last unless @line_item_selector
    qualifiers.compact.each do |qualifier|
      is_multi_select = qualifier.instance_variable_get(:@conditions).is_a?(Array)
      if is_multi_select
        qualifier.instance_variable_get(:@conditions).each do |nested_q|
          @post_amount_qualifier = nested_q if nested_q.is_a?(PostCartAmountQualifier)
          @qualifiers << qualifier
        end
      else
        @post_amount_qualifier = qualifier if qualifier.is_a?(PostCartAmountQualifier)
        @qualifiers << qualifier
      end
    end if @qualifiers.empty?
  end

  def qualifies?(cart)
    return true if @qualifiers.empty?
    @unmodified_line_items = cart.line_items.map do |item|
      new_item = item.dup
      new_item.instance_variables.each do |var|
        val = item.instance_variable_get(var)
        new_item.instance_variable_set(var, val.dup) if val.respond_to?(:dup)
      end
      new_item
    end if @post_amount_qualifier
    @qualifiers.send(@condition) do |qualifier|
      is_selector = false
      if qualifier.is_a?(Selector) || qualifier.instance_variable_get(:@conditions).any? { |q| q.is_a?(Selector) }
        is_selector = true
      end rescue nil
      if is_selector
        raise "Missing line item match type" if @li_match_type.nil?
        cart.line_items.send(@li_match_type) { |item| qualifier.match?(item) }
      else
        qualifier.match?(cart, @line_item_selector)
      end
    end
  end

  def run_with_hooks(cart)
    before_run(cart) if respond_to?(:before_run)
    run(cart)
    after_run(cart)
  end

  def after_run(cart)
    @discount.apply_final_discount if @discount && @discount.respond_to?(:apply_final_discount)
    revert_changes(cart) unless @post_amount_qualifier.nil? || @post_amount_qualifier.match?(cart)
  end

  def revert_changes(cart)
    cart.instance_variable_set(:@line_items, @unmodified_line_items)
  end
end

class QuantityLimit < Campaign
  def initialize(condition, customer_qualifier, cart_qualifier, line_item_selector, limit_by, limit)
    super(condition, customer_qualifier, cart_qualifier)
    @limit_by = limit_by
    @line_item_selector = line_item_selector
    @per_item_limit = limit
  end

  def run(cart)
    return unless qualifies?(cart)
    item_limits = {}
    to_delete = []
    if @per_item_limit == 0
      cart.line_items.delete_if { |item| @line_item_selector.nil? || @line_item_selector.match?(item) }
    else
      cart.line_items.each_with_index do |item, index|
        next unless @line_item_selector.nil? || @line_item_selector.match?(item)
        key = nil
        case @limit_by
          when :product
            key = item.variant.product.id
          when :variant
            key = item.variant.id
          when :cart
            key = 1
        end

        if key
          item_limits[key] = @per_item_limit if !item_limits.has_key?(key)
          needs_limiting = true if item.quantity > item_limits[key]
          needs_deleted = true if item_limits[key] <= 0
          max_amount = item.quantity - item_limits[key]
          item_limits[key] -= needs_limiting ? max_amount : item.quantity
        else
          needs_limiting = true if item.quantity > @per_item_limit
          max_amount = item.quantity - @per_item_limit
        end

        if needs_limiting
          if needs_deleted
            to_delete << index
          else
            item.split(take: max_amount)
          end
        end
      end

      if to_delete.length > 0
        del_index = -1
        cart.line_items.delete_if do |item|
          del_index += 1
          true if to_delete.include?(del_index)
        end
      end

    end
  end
end

class Selector
  def partial_match(match_type, item_info, possible_matches)
    match_type = (match_type.to_s + '?').to_sym
    if item_info.kind_of?(Array)
      possible_matches.any? do |possibility|
        item_info.any? do |search|
          search.send(match_type, possibility)
        end
      end
    else
      possible_matches.any? do |possibility|
        item_info.send(match_type, possibility)
      end
    end
  end
end

class ProductIdSelector < Selector
  def initialize(match_type, product_ids)
    @invert = match_type == :not_one
    @product_ids = product_ids.map { |id| id.to_i }
  end

  def match?(line_item)
    @invert ^ @product_ids.include?(line_item.variant.product.id)
  end
end

class VariantIdSelector < Selector
  def initialize(match_type, variant_ids)
    @invert = match_type == :not_one
    @variant_ids = variant_ids.map { |id| id.to_i }
  end

  def match?(line_item)
    @invert ^ @variant_ids.include?(line_item.variant.id)
  end
end

CAMPAIGNS = [
  QuantityLimit.new(
    :all,
    nil,
    nil,
    ProductIdSelector.new(
      :is_one,
      ##Limit Products to 1:
      limit_to_one
    ),
    :product,
    1
  ),
  QuantityLimit.new(
    :all,
    nil,
    nil,
    VariantIdSelector.new(
      :is_one,
      ## Limit Product Variants to 2:
      limit_to_two_variants
    ),
    :product,
    2
  ),
  QuantityLimit.new(
    :all,
    nil,
    nil,
    ProductIdSelector.new(
      :is_one,
      ##Limit Products to 2
      limit_to_two_prods
    ),
    :product,
    2
  ),
  QuantityLimit.new(
    :all,
    nil,
    nil,
    ProductIdSelector.new(
      :is_one,
      ##Limit Products to 5
      limit_to_five
    ),
    :product,
    5
  )

].freeze

CAMPAIGNS.each do |campaign|
  campaign.run_with_hooks(Input.cart)
end
# --------------------------------------#
# SWELL DISCOUNTS:
# --------------------------------------#
# This script is to be used for cart_fixed_amount redemptions OR free_product redemptions
swell_discount_amount_cents = nil
swell_discount_used = false

# shopify doesn't let us use any hash functions in scripts so had to implement some obfuscation
def calculate_token(rid)
  # will need to fill this in with your swell api key, reversed
  reverse_api_key = "ttw2r80RZj8YeiMxO7yDpaGk"
  buckets = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
  raw_token = reverse_api_key + rid.to_s.reverse
  token = ""
  rotate_amount = 1 + (rid.to_i % 59)

  raw_token.each_char do |char|
    ord = char.ord

    # "A" to "Z"
    if ord >= 65 and ord <= 90
      orig_index = ord-65
    elsif ord >= 97 and ord <= 122
      orig_index = ord - 97 + 26
    elsif ord >= 48 and ord <= 57
      orig_index = ord - 48 + 52
    else
      orig_index = false
    end

    if orig_index
      new_index = (orig_index + rotate_amount) % 62
      token += buckets[new_index]
    end
  end

  return token
end

# handle fixed amount discounts
# we store the discount amount on the first non-free item in the cart
# and then later we distribute the discount evenly across all remaining line items
Input.cart.line_items.each do |line_item|

  swell_points_used = line_item.properties["_swell_points_used"]
  swell_redemption_id = line_item.properties["_swell_redemption_id"]
  swell_redemption_token = line_item.properties["_swell_redemption_token"]
  swell_discount_type = line_item.properties["_swell_discount_type"]
  is_free_product = false

  if line_item.properties["_swell_discount_type"] && line_item.properties["_swell_discount_type"].eql?("cart_fixed_amount")
    if calculate_token(line_item.properties["_swell_redemption_id"]) == line_item.properties["_swell_redemption_token"]
      swell_discount_amount_cents = line_item.properties["_swell_discount_amount_cents"].to_i
      swell_discount_used = true
    end
  end

  # if there's a free product, reduce the price of this line item by the price of the product
  if swell_discount_type && swell_discount_type.eql?("product")
    if calculate_token(swell_redemption_id) == swell_redemption_token
      total_line_item_price_cents = line_item.line_price_was.cents
      each_item_price_cents = total_line_item_price_cents / line_item.quantity
      new_price = total_line_item_price_cents - each_item_price_cents
      line_item.change_line_price(Money.new(cents: new_price), message: "Rewards")
      is_free_product = true
    end
  end

  # if there's a fixed amount discount and this isn't a free product
  # distribute the fixed amount discount based on the percentage of the total this line item represents
  if swell_discount_used and !is_free_product
    price_cents = line_item.line_price_was.cents
    cart_cents = Input.cart.subtotal_price_was.cents
    discount_cents = (price_cents / cart_cents) * swell_discount_amount_cents
    line_item.change_line_price(Money.new(cents: price_cents - discount_cents), message: "Rewards")
  end

end



Output.cart = Input.cart