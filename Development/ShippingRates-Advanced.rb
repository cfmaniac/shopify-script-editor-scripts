# --------------------------------------#
# JH Last Update 11.20.2020
# --------------------------------------#
# ========================= Customizable Settings =============================
# This ShippingScript has been developed to replace the BetterShipping App with
# with better Shipping Processing, not reliant on a an External Shopify App.
# This Script includes the functionality to Stack perItem Shipping Costs, Deny
# International Shipping for Pro-Flora Product, and Local (to Satasota, FL) Free
# Shipping, as well as Free Shipping for Domestic Orders over $175.00
#
#
# Hide Rate(s) for Product/Country
#
# If the cart contains any "matching" items, and the shipping
# address country matches the entered country, the entered rate(s)
# are hidden.
#
#   - 'product_selector_match_type' determines whether we look for
#     products that do or don't match the entered selectors. Can
#     be:
#       - ':include' to check if the product does match
#       - ':exclude' to make sure the product doesn't match
#   - 'product_selector_type' determines how eligible products
#     will be identified. Can be either:
#       - ':tag' to find products by tag
#       - ':type' to find products by type
#       - ':vendor' to find products by vendor
#       - ':product_id' to find products by ID
#       - ':variant_id' to find products by variant ID
#       - ':all' for all products
#   - 'product_selectors' is a list of tags or IDs to identify
#     associated products
#   - 'country_selector_match_type' either:
#       - ':exclude' hide matching rates for all except country_codes list
#       - ':include' hide matching rates for all in country_codes list
#   - 'country_codes' is a list of country code abbreviations
#     - ie. United States would be `US`
#
#
#   - 'rate_match_type' determines whether the below strings
#     should be an exact or partial match. Can be:
#       - ':include' remove all rates from selector list
#       - ':exclude' remove all rates from execept selector list
#   - 'rate_match_modifier' determines whether the below strings
#     should be an exact or partial match. Can be:
#       - ':exact' for an exact match
#       - ':partial' for a partial match
#       - ':all' for all rates
#   - 'rate_names' is a list of strings to identify rates
#     - if using ':all' above, this can be set to 'nil'
#
#
# Can use the following ZipCode to Test For international Orders:
# To Test For International RULES:
# International UK:  WC2N 5DU
# International CA: A0G 1A0
# ================================================================
HIDE_RATES_FOR_PRODUCT_AND_COUNTRY = [
    {
        product_selector_match_type: :include,
        product_selector_type: :product_id,
        ## Restrict Internationally
        product_selectors: [
            "4381205954629", #PRO+FLORA (PRODUCTION)
            "5272319885477", #PRO+FLORA (DEVELOPMENT)
            "10678066127",   #ALPHA-AF (PRODUCTION)
            "4253037559877", #ALPHA-AF 2-PACK (PRODUCTION)
            "4253038346309", #ALPHA-AF 3-PACK (PRODUCTION)
            "5272322080933", #ALPHA-AF (DEVELOPMENT)
            "5272313692325", #ALPHA-AF 2-PACK (DEVELOPMENT)
            "5272312152229", #ALPHA-AF 3-PACK (DEVELOPMENT)
			      "4480432177221", #HAND-SANITIZER (PRODUCTION)
			      "5781053341861"  #HAND-SANITIZER (DEVELOPMENT)
			],
        country_selector_match_type: :exclude,
        country_codes: ["US"],
        rate_match_type: :include,
        rate_match_modifier: :all,
        rate_names: nil,
    },
# {
#   product_selector_match_type: :include,
#   product_selector_type: :product_id,
#   product_selectors: [4381205954629],
#   country_selector_match_type: :include,
#   country_codes: ["US"],
#   rate_match_type: :exclude,
#   rate_match_modifier: :exact,
#   rate_names: ["UPS 2nd Day Air®"],
# },
]
# =========================== CONSTANTS =====================================
# DEFAULT RATE OFFSET:
CUSTOM_UPPER_RATE_OFFSET = 1000
#Free Shipping for Local Zipcodes
FREE_LOCAL_ZIPS = [
    '34284', '34285', '34292', '34293',
    '34201', '34202', '34204', '34205',
    '34206', '34210', '34211', '34203',
    '34207', '34208', '34209', '34212',
    '34282', '34280', '34281', '34231',
    '34232', '34233', '34234', '34235',
    '34236', '34237', '34238', '34239',
    '34240', '34241', '34242', '34243' ]
#Custom Shipping Rates
CUSTOM_SHIPPING_RATES = {
    :sample_rate => 5.99,
    :bundle_rate => 9.99
}
# ================================ Script Code (do not edit) ================================
# ================================================================
# ProductSelector
#
# Finds matching products by the entered criteria
# by utilizing Product Tags (tagged as either 'Bundle' or 'Sample')
# ================================================================
class ProductSelector
  def initialize(match_type, selector_type, selectors)
    @match_type = match_type
    @comparator = match_type == :include ? 'any?' : 'none?'
    @selector_type = selector_type
    @selectors = selectors
  end
  def match?(line_item)
    if @selector_type == :tag
      product_tags = line_item.variant.product.tags.map { |tag| tag.downcase.strip }
      @selectors = @selectors.map { |selector| selector.downcase.strip }
      (@selectors & product_tags).send(@comparator)
    elsif @selector_type == :type
      @selectors = @selectors.map { |selector| selector.downcase.strip }
      (@match_type == :include) == @selectors.include?(line_item.variant.product.product_type.downcase.strip)
    elsif @selector_type == :vendor
      @selectors = @selectors.map { |selector| selector.downcase.strip }
      (@match_type == :include) == @selectors.include?(line_item.variant.product.vendor.downcase.strip)
    elsif @selector_type == :product_id
      (@match_type == :include) == @selectors.include?(line_item.variant.product.id)
    elsif @selector_type == :variant_id
      (@match_type == :include) == @selectors.include?(line_item.variant.id)
    elsif @selector_type == :all
      true
    else
      raise RuntimeError.new('Invalid product selector type')
    end
  end
end
# ================================================================
# CountrySelector
#
# Finds whether the supplied country code matches the entered
# string
# ================================================================
class CountrySelector
  def initialize(countries, country_match_type)
    @match_type = country_match_type
    @countries = countries.map { |country| country.upcase.strip }
  end
  def match?(country_code)
    if (@match_type == :include)
      @countries.any? { |country| country_code.upcase.strip == country }
    elsif (@match_type == :exclude)
      @countries.any? { |country| country_code.upcase.strip != country }
    else
      raise RuntimeError.new('Invalid country selector type')
    end
  end
end
# ================================================================
# RateNameSelector
#
# Finds whether the supplied rate name matches any of the entered
# names
# ================================================================
class RateNameSelector
  def initialize(match_type, match_modifier, rate_names)
    @match_type = match_type
    @match_modifier = match_modifier
    # @comparator = match_type == :exact ? '==' : 'include?'
    if (match_type == :exclude )
      @comparator = match_modifier == :exact ? '==' : 'include?'
    elsif(match_type == :include )
      @comparator = match_modifier == :exact ? '!=' : 'none?'
    end
    @rate_names = rate_names&.map { |rate_name| rate_name.downcase.strip }
  end
  def match?(shipping_rate)
    if @match_modifier == :all
      true
    else
      if (@match_type == :exclude)
        if @match_modifier == :all
          true
        else
          @rate_names.any? { |name| shipping_rate.name.downcase.send(@comparator, name) }
        end
      else
        @rate_names.any? { |name| shipping_rate.name.downcase.send(@comparator, name) }
      end
    end
  end
end
# ================================================================
# HideRatesForProductCountryCampaign
#
# If the cart contains any "matching" items, and the shipping
# address country matches the entered country, the entered rate(s)
# are hidden.
# ================================================================
class HideRatesForProductCountryCampaign
  def initialize(campaigns)
    @campaigns = campaigns
  end
  def run(cart, shipping_rates)
    address = cart.shipping_address
    return if address.nil?
    @campaigns.each do |campaign|
      product_selector = ProductSelector.new(
          campaign[:product_selector_match_type],
          campaign[:product_selector_type],
          campaign[:product_selectors],
          )
      country_selector = CountrySelector.new(campaign[:country_codes], campaign[:country_selector_match_type])
      product_match = cart.line_items.any? { |line_item| product_selector.match?(line_item) }
      country_match = country_selector.match?(address.country_code)
      next unless product_match && country_match
      rate_name_selector = RateNameSelector.new(
          campaign[:rate_match_type],
          campaign[:rate_match_modifier],
          campaign[:rate_names],
          )
      shipping_rates.delete_if do |shipping_rate|
        rate_name_selector.match?(shipping_rate)
      end
    end
  end
end
# ================================================================
# AdjustRatesForCustomPricingByLineItemCampaign
#
# If the cart contains any "matching" items, and the shipping
# address zip/country matches, the current rate(s)
# are offset to a custom rate and aggregated.
# ================================================================
class AdjustRatesForCustomPricingByLineItemCampaign
  def run(cart, shipping_rates)
    destination_addr_zip = cart.shipping_address.zip
    isShippingLocal = FREE_LOCAL_ZIPS.include? destination_addr_zip

    return if destination_addr_zip.nil?

    shipping_rates.each do |rate|
      next unless rate.source == "shopify"

      if isShippingLocal
        rate.change_name('Standard')
        rate.change_price(Money.new(cents: 0), message: 'Free Local Shipping')
        next
      else
        rate_sum = self.sum_custom_rate(cart.line_items)
      end

      if rate.price < Money.new(cents: CUSTOM_UPPER_RATE_OFFSET * 100)
        # skip when the cart has no custom items
        # we know because current rate is less than the custom offset rate of $1000
        next
      elsif rate_sum > 0 and rate_sum < CUSTOM_UPPER_RATE_OFFSET # calculated rate is between 0 - 1000
        offset = Money.new(cents: (CUSTOM_UPPER_RATE_OFFSET - rate_sum) * 100)

        chng_price = rate.price - offset

        rate.change_name('Standard')
        rate.change_price(chng_price, message: 'Just pay the shipping')
      end

    end
  end

  private

  def sum_custom_rate(line_items)
    rate_sum = 0
    line_items.each do |item|
      # Calculate the custom rate offset based on each line item quanity if tag matches
      if item.variant.product.tags.include? 'sample_just_shipping'
        rate_sum += item.quantity * CUSTOM_SHIPPING_RATES[:sample_rate]
      elsif item.variant.product.product_type === 'Bundle'
        rate_sum += item.quantity * CUSTOM_SHIPPING_RATES[:bundle_rate]
      end
    end
    return rate_sum
  end
end

CAMPAIGNS = [
    HideRatesForProductCountryCampaign.new(HIDE_RATES_FOR_PRODUCT_AND_COUNTRY),
    AdjustRatesForCustomPricingByLineItemCampaign.new()
]

CAMPAIGNS.each do |campaign|
  campaign.run(Input.cart, Input.shipping_rates)
end

Output.shipping_rates = Input.shipping_rates
